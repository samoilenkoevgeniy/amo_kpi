/**
 * Created by evgeniy_pc on 18.04.2016.
 */
$(window).ready(function(){

    var date_picker = $("#datepicker");
    date_picker.datepicker({
        multidate : true
    }).on("changeDate", function(e){
        console.log(e.dates);
        console.log(e.dates.length);
        if(e.dates.length == 2) {
            var url = "",
                prefix = "?";
            for (var i = 0 in e.dates) {
                var date = new Date(e.dates[i]);

                if(i > 0 && prefix == "?") {
                    prefix = "&";
                }
                url += prefix+"date[]="+date.toDateString();
            }
            window.location.href = url;
        }
        /*var date = new Date(e.date);
        var url = date_picker.data("url-fallback");

        window.location.href = url+"?timestamp="+date.toDateString();*/
    });

    $(".money").each(function(){
        var that = $(this);
        var new_val = that.text().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        that.text(new_val);
    });

});