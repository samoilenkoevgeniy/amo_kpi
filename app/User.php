<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', "role_id", "amo_id"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function calls()
    {
        return $this->hasMany(UserCall::class, "user_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deals()
    {
        return $this->hasMany(Deal::class);
    }

    /**
     * @param int $amo_id
     * @return bool|string
     */
    public static function getUserByAmoId($amo_id = 0)
    {
        $user_id = \Redis::get("user.amo_id.".$amo_id);
        if(!$user_id) {
            $user = User::whereAmoId($amo_id)->first();
            if($user) {
                \Redis::set("user.amo.id.".$user->amo_id, $user->id);
                $user_id = $user->id;
            }
        }
        return $user_id;
    }
}
