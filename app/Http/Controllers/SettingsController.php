<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

use App\Http\Requests;

class SettingsController extends Controller
{
    protected $fields = [
        "dialing_success_criteria" => [
            "rus_name" => "Критерий дозвона (в секундах)"
        ],
        "success_criteria" => [
            "rus_name" => "Критерий успешности звонка (в секундах)"
        ],
        "pay_dialing" => [
            "rus_name" => "Оплата за успешный дозвон"
        ],
        "pay_dialing_success" => [
            "rus_name" => "Оплата за успешный разговор"
        ],
        "pay_successful_conversation" => [
            "rus_name" => "Сколько платим за 1 минуту успешного разговор"
        ],
        "time_work_day_start" => [
            "rus_name" => "Время начала рабочего дня"
        ],
        "time_work_day_end" => [
            "rus_name" => "Время окончания рабочего дня"
        ],
        "max_average_diff" => [
            "rus_name" => "Норматив среднего времени между звонками"
        ],
        "penalty_average_diff" => [
            "rus_name" => "Штраф за каждую минуту просрока"
        ],
        "penalty_first_call" => [
            "rus_name" => "Штраф за просрок первого звонка"
        ],
        "penalty_last_call" => [
            "rus_name" => "Штраф за просрок последнего звонка звонка"
        ],
        "penalty_deal_without_tasks" => [
            "rus_name" => "Штраф за открытую сделку без задачи"
        ],
        "penalty_overdue_task" => [
            "rus_name" => "Штраф за просроченую задачу"
        ],
    ];

    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fields = $this->fields;
        return response()->view("administration.settings.index", compact("fields"));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        foreach($this->fields as $key => $field) {
            $value = $request->input($key);
            \Redis::set("system.settings.".$key, $value);
            Setting::set($key, $value);
        }
        \Session::put("status", "Вы успешно сохранили настройки системы!");
        return redirect()->back();
    }
}
