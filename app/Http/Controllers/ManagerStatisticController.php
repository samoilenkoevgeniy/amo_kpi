<?php

namespace App\Http\Controllers;

use App\DailyStatistic;
use App\UserCall;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class ManagerStatisticController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $today = $request->has("today") ? 1 : 0;
        $dates = $request->has("date") ? $request->input("date") : "";
        $timestamp = "";
        $date_for_view = "";
        $total_penalty = 0;
        $total_salary_diff = 0;

        if($dates) {
            foreach($dates as $date) {
                $date = Carbon::parse($date);
                $date->hour = 0;
                $date->minute = 0;
                $date->second = 0;
                $date_for_view[] = explode(" ", $date->toDateTimeString())[0];
                $date_start[] = $date->subMinute(1)->toDateTimeString();
                $date_end[] = $date->addDay()->toDateTimeString();
            }
            $date_start = $date_start[0];
            $date_end = $date_end[count($date_end)-1];
            $date_for_view = explode(" ", $date_for_view[0])[0] . " - " . explode(" ", $date_for_view[count($date_for_view)-1])[0];

        } else {
            $date_for_view = "";
            $date_start = "";
            $date_end = "";
        }

        if($today) {
            $date = Carbon::now();
            $date->hour = 0;
            $date->minute = 0;
            $date->second = 0;
            $date_for_view = explode(" ", $date->toDateTimeString())[0];
            $date_start = $date->subMinute(1)->toDateTimeString();
            $date_end = $date->addDay()->toDateTimeString();
        }

        if(!$request->has("id")) {
            $id = \Auth::user()->id;
        } else {
            $id = $request->input("id");
        }
        $calls = UserCall::whereUserId($id)->where(function($query) use ($date_start, $date_end){
            if($date_start != "" && $date_end != "") {
                $query->where("date", ">", $date_start);
                $query->where("date", "<", $date_end);
            }
        })->orderBy("date", "DESC");

        $all_calls = UserCall::where(function($query) use ($date_start, $date_end){
            if($date_start != "" && $date_end != "") {
                $query->where("date", ">", $date_start);
                $query->where("date", "<", $date_end);
            }
        })->get()->count();
        $dialing_calls_all = UserCall::whereDialing(1)->where(function($query) use ($date_start, $date_end){
            if($date_start != "" && $date_end != "") {
                $query->where("date", ">", $date_start);
                $query->where("date", "<", $date_end);
            }
        })->get()->count();
        $success_calls_total = UserCall::whereSuccess(1)->where(function($query) use ($date_start, $date_end){
            if($date_start != "" && $date_end != "") {
                $query->where("date", ">", $date_start);
                $query->where("date", "<", $date_end);
            }
        })->get()->count();
        $success_calls_total_duration = UserCall::whereSuccess(1)->where(function($query) use ($date_start, $date_end){
            if($date_start != "" && $date_end != "") {
                $query->where("date", ">", $date_start);
                $query->where("date", "<", $date_end);
            }
        })->sum("duration");
        $calls_total_duration = UserCall::where(function($query) use ($date_start, $date_end){
            if($date_start != "" && $date_end != "") {
                $query->where("date", ">", $date_start);
                $query->where("date", "<", $date_end);
            }
        })->sum("duration");

        $data_pie = [
            [
                "value" => $all_calls,
                "color" => "#FF3400",
                "highlight" => "#FF3400",
                "label" => "Всего звонков"
            ],
            [
                "value" => $dialing_calls_all,
                "color" => "#FFDB0C",
                "highlight" => "#FFDB0C",
                "label" => "Дозвонов"
            ],
            [
                "value" => $success_calls_total,
                "color" => "#93FF51",
                "highlight" => "#93FF51",
                "label" => "Успешных звонков"
            ]
        ];

        $data_pie_2 = [
            [
                "value" => round($calls_total_duration/60, 2),
                "color" => "#FF3400",
                "highlight" => "#FF3400",
                "label" => "Всего минут"
            ],
            [
                "value" => round($success_calls_total_duration/60, 2),
                "color" => "#93FF51",
                "highlight" => "#93FF51",
                "label" => "Минут успешных звонков"
            ]
        ];
        $data_pie = json_encode($data_pie);
        $data_pie_2 = json_encode($data_pie_2);

        if($today || $timestamp != "") {
            $calls =  $calls->get();
            $count_calls = $calls->count();
            $diff = 0;
            if($count_calls > 1) {
                for($i = 0; $i != $count_calls-1; $i++) {
                    $dt = Carbon::createFromFormat("Y-m-d H:i:s", $calls[$i]['date']);
                    $dt_next = Carbon::createFromFormat("Y-m-d H:i:s", $calls[$i+1]['date']);
                    $diff += $dt->diffInMinutes($dt_next);
                }
            }
            $average_diff = $count_calls ? round($diff/$count_calls, 2) : 0;
        } else {
            $calls = $calls->paginate(10);
        }

        $total_salary = round(($dialing_calls_all*(int)\Redis::get("system.settings.pay_dialing")) +
            ($success_calls_total*(int)\Redis::get("system.settings.pay_dialing_success")) +
            (($success_calls_total_duration/60)*(int)\Redis::get("system.settings.pay_successful_conversation"))
        , 2);

        if(isset($date)) {
            $date = explode(" ", $date)[0];
        } else {
            $date = "";
        }

        if($today || $timestamp) {

            $dailyStatistic = DailyStatistic::getTodayStatistic($id, $date);
            if($dailyStatistic) {
                $total_penalty = $dailyStatistic->total_penalty;
                $total_salary_diff = $total_salary-$total_penalty;
            }
        } else {
            $total_penalty = DailyStatistic::whereUserId($id)->sum("total_penalty");
            $total_salary_diff = $total_salary-$total_penalty;
        }

        /*
         * @todo предусмотреть, что статистики еще нет может быть
         * */
        return response()->view("manager.statistics.index", compact(
            "calls", "today", "timestamp", "date_for_view",
            "data_pie", "total_salary", "data_pie_2", "average_diff",
            "total_salary_diff", "total_penalty", "dailyStatistic"
        ));
    }
}
