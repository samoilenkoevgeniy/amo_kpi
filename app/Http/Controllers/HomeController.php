<?php

namespace App\Http\Controllers;

use App\DailyStatistic;
use App\Http\Requests;
use App\User;
use App\UserCall;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $today = $request->has("today") ? 1 : 0;
        $dates = $request->has("date") ? $request->input("date") : "";
        $timestamp = '';
        $date = "";

        if($dates) {
            foreach($dates as $date) {
                $date = Carbon::parse($date);
                $date->hour = 0;
                $date->minute = 0;
                $date->second = 0;
                $date_for_view[] = explode(" ", $date->toDateTimeString())[0];
                $date_start[] = $date->subMinute(1)->toDateTimeString();
                $date_end[] = $date->addDay()->toDateTimeString();
            }
            $date_start = $date_start[0];
            $date_end = $date_end[count($date_end)-1];
            $date_for_view = explode(" ", $date_for_view[0])[0] . " - " . explode(" ", $date_for_view[count($date_for_view)-1])[0];
        } else {
            $date_for_view = "";
            $date_start = '';
            $date_end = '';
        }
        if($today) {
            $date = Carbon::now();
            $date->hour = 0;
            $date->minute = 0;
            $date->second = 0;
            $date_for_view = explode(" ", $date->toDateTimeString())[0];
            $date_start[] = $date->subMinute(1)->toDateTimeString();
            $date_end[] = $date->addDay()->toDateTimeString();
        }

        $users = User::all();
        $data_chart = [];
        $data = [];
        $bar_chart_call_all = [];
        $bar_chart_call_success = [];
        $labels_bar_chart = "";
        foreach($users as $user) {
            $calls = UserCall::whereUserId($user->id)->where(function($query) use ($date_start, $date_end){
                if($date_start != "" && $date_end != "") {
                    $query->where("date", ">", $date_start);
                    $query->where("date", "<", $date_end);
                }
            })->get();
            $count_calls = $calls->count();
            $count_calls_success = UserCall::whereUserId($user->id)->whereSuccess(1)->where(function($query) use ($date_start, $date_end){
                if($date_start != "" && $date_end != "") {
                    $query->where("date", ">", $date_start);
                    $query->where("date", "<", $date_end);
                }
            })->get()->count();
            $dialing = UserCall::whereUserId($user->id)->whereDialing(1)->where(function($query) use ($date_start, $date_end){
                if($date_start != "" && $date_end != "") {
                    $query->where("date", ">", $date_start);
                    $query->where("date", "<", $date_end);
                }
            })->get()->count();
            $duration_success_calls = round(UserCall::whereUserId($user->id)->whereSuccess(1)->where(function($query) use ($date_start, $date_end){
                if($date_start != "" && $date_end != "") {
                    $query->where("date", ">", $date_start);
                    $query->where("date", "<", $date_end);
                }
            })->get()->sum("duration")/60, 2);
            $diff = 0;
            if($count_calls > 1) {
                for($i = 0; $i != $count_calls-1; $i++) {
                    $dt = Carbon::createFromFormat("Y-m-d H:i:s", $calls[$i]['date']);
                    $dt_next = Carbon::createFromFormat("Y-m-d H:i:s", $calls[$i+1]['date']);
                    $diff += $dt->diffInMinutes($dt_next);
                }
            }
            $average_diff = $count_calls ? $diff/$count_calls : 0;
            //считаем качество базы
            $quality_base = $dialing ? round(($count_calls_success/$dialing)*100, 2) : 0;
            //не дозвоны
            $not_dialing = $count_calls-$dialing;

            $labels_bar_chart .= '"'.$user->name.'",';
            $bar_chart_call_all[] = $count_calls;
            $bar_chart_call_success[] = $count_calls_success;
            $color =  HomeController::getColor($user->id);
            $data_chart[] = [
                "value" => $count_calls,
                "color" => $color,
                "highlight" => $color,
                "label" => $user->name
            ];
            $first_call = "";
            $last_call = "";

            if($date != "") {
                $dailyStatistic = DailyStatistic::where("date", "=", $date->format("Y-m-d"))->whereUserId($user->id)->first();
                if($dailyStatistic) {
                    $first_call = $dailyStatistic->first_call;
                    $last_call = $dailyStatistic->last_call;
                }

            }
            $total_penalty = DailyStatistic::whereUserId($user->id)->where(function($query) use ($date){
                if($date != "") {
                    $query->where("date", "=", $date->format("Y-m-d"));
                }
            })->sum("total_penalty");
            $total_salary = DailyStatistic::whereUserId($user->id)->where(function($query) use ($date){
                if($date != "") {
                    $query->where("date", "=", $date->format("Y-m-d"));
                }
            })->where(function($query){

            })->sum("total_salary");

            $data[] = [
                "user_name" => $user->name,
                "count_calls" => $count_calls,
                "count_calls_success" => $count_calls_success,
                "dialing" => $dialing,
                "not_dialing" => $not_dialing,
                "duration_success_calls" => $duration_success_calls,
                "average_diff" => round ($average_diff),
                "average_length_success_calls" => $count_calls_success ? round ($duration_success_calls/$count_calls_success, 2) : 0,
                "first_call" => $first_call,
                "last_call" => $last_call,
                "new_row" => 1,
                "quality_base" => $quality_base,
                "total_penalty" => $total_penalty,
                "total_salary" => $total_salary,
            ];
        }
        usort($data, function($a, $b){
            return ($a['total_salary'] < $b['total_salary']);
        });
        $labels_bar_chart = mb_substr($labels_bar_chart, 0, -1);

        $data_chart = json_encode($data_chart);
        $bar_chart_call_all = json_encode($bar_chart_call_all);
        $bar_chart_call_success = json_encode($bar_chart_call_success);
        $all_calls = UserCall::where(function($query) use ($date_start, $date_end){
            if($date_start != "" && $date_end != "") {
                $query->where("date", ">", $date_start);
                $query->where("date", "<", $date_end);
            }
        })->get()->count();
        $dialing_calls_all = UserCall::whereDialing(1)->where(function($query) use ($date_start, $date_end){
            if($date_start != "" && $date_end != "") {
                $query->where("date", ">", $date_start);
                $query->where("date", "<", $date_end);
            }
        })->get()->count();
        $success_calls_total = UserCall::whereSuccess(1)->where(function($query) use ($date_start, $date_end){
            if($date_start != "" && $date_end != "") {
                $query->where("date", ">", $date_start);
                $query->where("date", "<", $date_end);
            }
        })->get()->count();
        $success_calls_total_duration = UserCall::whereSuccess(1)->where(function($query) use ($date_start, $date_end){
            if($date_start != "" && $date_end != "") {
                $query->where("date", ">", $date_start);
                $query->where("date", "<", $date_end);
            }
        })->sum("duration");
        $calls_total_duration = UserCall::where(function($query) use ($date_start, $date_end){
            if($date_start != "" && $date_end != "") {
                $query->where("date", ">", $date_start);
                $query->where("date", "<", $date_end);
            }
        })->sum("duration");
        $total_quality_base = ($dialing_calls_all) ? round(($success_calls_total/$dialing_calls_all)*100, 2) : 0;
        $data_pie = [
            [
                "value" => $all_calls,
                "color" => "#FF3400",
                "highlight" => "#FF3400",
                "label" => "Всего звонков"
            ],
            [
                "value" => $dialing_calls_all,
                "color" => "#FFDB0C",
                "highlight" => "#FFDB0C",
                "label" => "Дозвонов"
            ],
            [
                "value" => $success_calls_total,
                "color" => "#93FF51",
                "highlight" => "#93FF51",
                "label" => "Успешных звонков"
            ]
        ];
        $data_pie_2 = [
            [
                "value" => round($calls_total_duration/60, 2),
                "color" => "#FF3400",
                "highlight" => "#FF3400",
                "label" => "Всего минут"
            ],
            [
                "value" => round($success_calls_total_duration/60, 2),
                "color" => "#93FF51",
                "highlight" => "#93FF51",
                "label" => "Минут успешных звонков"
            ]
        ];
        $data_pie = json_encode($data_pie);
        $data_pie_2 = json_encode($data_pie_2);

        //
        return response()->view('welcome', compact(
            "data_chart", "data", "labels_bar_chart", "bar_chart_call_all",
            "bar_chart_call_success", "today", "date_for_view", "timestamp",
            "data_pie","data_pie_2", "total_quality_base"
        ));
    }

    /**
     * @param int $user_id
     * @return string
     */
    public static function getColor($user_id = 0)
    {
        $range = 192 - 64;
        $factor = $range / 256;
        $offset = 64;

        $base_hash = substr(md5(time().$user_id), 0, 6);
        $b_R = hexdec(substr($base_hash,0,2));
        $b_G = hexdec(substr($base_hash,2,2));
        $b_B = hexdec(substr($base_hash,4,2));

        $f_R = floor((floor($b_R * $factor) + $offset) / 16) * 16;
        $f_G = floor((floor($b_G * $factor) + $offset) / 16) * 16;
        $f_B = floor((floor($b_B * $factor) + $offset) / 16) * 16;

        return sprintf('#%02x%02x%02x', $f_R, $f_G, $f_B);
    }
}
