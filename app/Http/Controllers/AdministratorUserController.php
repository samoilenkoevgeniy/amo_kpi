<?php

namespace App\Http\Controllers;

use App\Amo;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;

class AdministratorUserController extends Controller
{

    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users  = User::all();
        return response()->view("administration.users.index", compact("users"));
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function dropPassword($id = 0)
    {
        $user = User::find($id);
        $newPassword = substr(md5(time().$user->id), 0, 12);
        $user->password = \Hash::make($newPassword);
        $user->save();
        if(ENV("EMAIL", true)) {
            \Mail::send('emails.newPassword', ['user' => $user, "newPassword" => $newPassword], function ($m) use ($user) {
                $m->from('noreply@'.ENV("DOMAIN"), ENV("DOMAIN"));
                $m->to($user->email, $user->name)->subject('Новый пароль!');
            });
        }

        \Session::put("status", "Вы успешно сбросили пароль для пользователя ".$user->name);
        return redirect()->to("/administration/users");
    }
}
