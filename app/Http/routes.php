<?php

Route::group(["middleware" => ["web", "auth"]], function() {

    Route::get("/", "HomeController@index");

    Route::group(["middleware" => "administrator", "prefix" => "administration"], function(){
        Route::group(["prefix" => "settings"], function(){
            Route::get("/", "SettingsController@index");
            Route::post("/update", "SettingsController@update");
        });
        Route::group(["prefix" => "users"], function(){
            Route::get("/", "AdministratorUserController@index");
            Route::get("/dropPassword/{id}", "AdministratorUserController@dropPassword");
        });
    });

    Route::group(["prefix" => "manager"], function(){
        Route::group(["as" => "statistics"], function(){
            Route::get("/statistics", "ManagerStatisticController@index");
        });
    });

    Route::get('/home', function(){
        return redirect()->to("/");
    });
});

Route::group(["middleware" => "web"], function(){

    Route::auth();
    Route::get('register', function(){
        return redirect()->to("/");
    });
    Route::post('register', function(){
        return redirect()->to("/");
    });

    Route::get("/test", function(){

        $user = App\User::find(3);
        $user->password = Hash::make("123123");
        $user->save();

    });


    Route::get("/getCalls/sfdkfksdflsdflksdf", function(){
        \App\Amo::getCalls();
    });

    Route::get("/initial", function(){
        set_time_limit(0);


        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('synchronizations')->truncate();
        DB::table('daily_statistics')->truncate();
        DB::table('user_calls')->truncate();
        DB::table('tasks')->truncate();
        DB::table('deals')->truncate();
        DB::table('users')->truncate();
        App\User::create([
            "name" => "Администратор",
            "email" => "admin@system.ru",
            "password" => Hash::make("123123"),
            "role_id" => 1
        ]);

        \App\Amo::getUsers();

        \App\Amo::auth();
        \App\Amo::get_events_all();

        \App\Amo::getAllDeals();
        \App\Amo::getAllTasks();
        \App\Amo::getOldDailyStatistics();

        App\Synchronization::create([
            "last_sync" => date("D, d M Y H:i:s")
        ]);
    });
});
