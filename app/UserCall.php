<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserCall extends Model
{
    protected $fillable = [
        "date", "user_id", "external_id",
        "link", "duration", "incoming",
        "amo_id","phone", "success",
        "dialing"
    ];

    /**
     * @return float
     */
    public function getCostCall()
    {
        $result = 0;
        if($this->dialing) {
            $result += \Redis::get("system.settings.pay_dialing");
        }

        if($this->success) {
            $pay_successful_conversation = (int)\Redis::get("system.settings.pay_successful_conversation");
            $result += \Redis::get("system.settings.pay_dialing_success");
            $result += ($this->duration/60)*$pay_successful_conversation;
        }

        return round($result, 2);
    }

    /**
     * @param $value
     */
    public function setDateAttribute($value)
    {
        $date = Carbon::createFromFormat("Y-m-d H:i:s", $value);
        $date->hour += 3;
        $this->attributes["date"] = $date->format("Y-m-d H:i:s");
    }
}
