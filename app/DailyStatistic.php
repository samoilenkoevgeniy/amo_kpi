<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyStatistic extends Model
{
    protected $fillable = [
        "date", "average_diff", "first_call", "last_call",
        "open_deals_with_out_tasks", "overdue_tasks", "open_tasks", "user_id",
        "total_penalty", "total_salary"
    ];

    /**
     * @param int $user_id
     * @param string $date
     * @return mixed
     */
    public static function getTodayStatistic($user_id = 0, $date = "")
    {
        if(!$date) {
            $date = date("Y-m-d");
        }
        return DailyStatistic::where("date", "=", $date)->whereUserId($user_id)->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
