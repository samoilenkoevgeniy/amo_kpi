<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        "text", "user_id", "date_create", "date_last_modify", "closed", "task_type", "deal_id", "date_due"
    ];
}
