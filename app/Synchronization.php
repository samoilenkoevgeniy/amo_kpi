<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Synchronization extends Model
{
    protected $fillable = [
        "last_sync", "count_incoming", "count_outgoing"
    ];

    public static function getLast()
    {
        $value = Synchronization::orderBy('created_at', 'desc')->first();
        return $value ? $value->created_at : "";
    }
}