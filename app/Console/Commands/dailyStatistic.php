<?php

namespace App\Console\Commands;

use App\Deal;
use App\Task;
use App\UserCall;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Auth\User;

class dailyStatistic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statistic:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'to write down daily statistic';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(date("H:i") == \Redis::get("system.settings.time_work_day_end")) {
            $users = User::all();
            foreach($users as $user) {
                $total_penalty = 0;
                $diff = 0;
                $max_average_diff = (int)\Redis::get("system.settings.max_average_diff");
                $date_start = date("Y-m-d ").\Redis::get("system.settings.time_work_day_start").":00";
                $date_end = date("Y-m-d ").\Redis::get("system.settings.time_work_day_end").":00";

                $calls = UserCall::whereUserId($user->id)->where(function($query) use ($date_start, $date_end) {
                    $query->where("date", ">", $date_start);
                    $query->where("date", "<", $date_end);
                })->get();
                $count_calls = $calls->count();

                if($count_calls > 1) {
                    for($i = 0; $i != $count_calls-1; $i++) {
                        $dt = Carbon::createFromFormat("Y-m-d H:i:s", $calls[$i]['date']);
                        $dt_next = Carbon::createFromFormat("Y-m-d H:i:s", $calls[$i+1]['date']);
                        $diff += $dt->diffInMinutes($dt_next);
                    }
                }

                $average_diff = $count_calls ? $diff/$count_calls : 0;

                if($calls->count() > 0) {

                    $first_call = explode(" ", $calls->first()->date)[1];
                    $last_call = explode(" ", $calls->last()->date)[1];

                    $dt_first_call = Carbon::createFromFormat("Y-m-d H:i:s",date("Y-m-d ").$first_call);
                    $dt_last_call = Carbon::createFromFormat("Y-m-d H:i:s",date("Y-m-d ").$last_call);

                    $dt_day_start = Carbon::createFromFormat("Y-m-d H:i:s", $date_start);
                    $dt_day_end = Carbon::createFromFormat("Y-m-d H:i:s", $date_end);

                    $diff_first_call = $dt_day_start->diffInMinutes($dt_first_call);
                    $diff_last_call = $dt_day_end->diffInMinutes($dt_last_call);

                    $total_penalty += $diff_first_call*\Redis::get("system.settings.penalty_first_call");
                    $total_penalty += $diff_last_call*\Redis::get("system.settings.penalty_last_call");
                } else {
                    $first_call = NULL;
                    $last_call = NULL;
                }

                if($average_diff > $max_average_diff) {
                    $penalty_time = abs($max_average_diff-$average_diff);
                    $total_penalty += $penalty_time*(int)\Redis::get("system.settings.penalty_average_diff");
                }

                $deals = Deal::whereUserId($user->id)->where(function($query) use ($date_start, $date_end) {
                    $query->where("date_create", ">", $date_start);
                    $query->where("date_create", "<", $date_end);
                })->whereClosed(0)->get();

                $count_open_deals_without_tasks = 0;
                foreach($deals as $deal) {
                    if($deal->tasks->count() == 0) {
                        $count_open_deals_without_tasks++;
                    }
                }

                $dialing_calls_all = UserCall::whereDialing(1)->whereUserId($user->id)->where(function($query) use ($date_start, $date_end){
                    if($date_start != "" && $date_end != "") {
                        $query->where("date", ">", $date_start);
                        $query->where("date", "<", $date_end);
                    }
                })->get()->count();

                $success_calls_total = UserCall::whereSuccess(1)->whereUserId($user->id)->where(function($query) use ($date_start, $date_end){
                    if($date_start != "" && $date_end != "") {
                        $query->where("date", ">", $date_start);
                        $query->where("date", "<", $date_end);
                    }
                })->get()->count();

                $success_calls_total_duration = UserCall::whereSuccess(1)->whereUserId($user->id)->where(function($query) use ($date_start, $date_end){
                    if($date_start != "" && $date_end != "") {
                        $query->where("date", ">", $date_start);
                        $query->where("date", "<", $date_end);
                    }
                })->sum("duration");

                $total_salary = round(($dialing_calls_all*(int)\Redis::get("system.settings.pay_dialing")) +
                    ($success_calls_total*(int)\Redis::get("system.settings.pay_dialing_success")) +
                    (($success_calls_total_duration/60)*(int)\Redis::get("system.settings.pay_successful_conversation"))
                    , 2);

                $total_penalty+= $count_open_deals_without_tasks*\Redis::get("system.settings.penalty_deal_without_tasks");

                $overdue_tasks = Task::whereRaw("date_due < NOW()")->whereClosed(0)->whereUserId($user->id)->get()->count();
                $total_penalty+= $overdue_tasks*\Redis::get("system.settings.penalty_overdue_task");

                \App\DailyStatistic::create([
                    "date" => date("Y-m-d"),
                    "average_diff" => $average_diff,
                    "first_call" => $first_call,
                    "last_call" => $last_call,
                    "user_id" => $user->id,
                    "open_deals_with_out_tasks" => $count_open_deals_without_tasks,
                    "overdue_tasks" => $overdue_tasks,
                    "total_penalty" => $total_penalty,
                    "total_salary" => $total_salary
                ]);
                $this->info("cool!");
            }
        }

        $this->info("Successful!");
    }
}
