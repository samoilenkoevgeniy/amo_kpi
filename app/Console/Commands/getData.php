<?php

namespace App\Console\Commands;

use App\Amo;
use App\Synchronization;
use Illuminate\Console\Command;

class getData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Data from AmoCRM';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date("D, d M Y H:i:s");
        $last_sync = Synchronization::latest()->first();
        if($last_sync) {
            $date = $last_sync->last_sync;
        }

        Amo::auth();
        Amo::getCallsConsole($date);
        Amo::getDealsAndTasks($date);

        Synchronization::create([
            "last_sync" => date("D, d M Y H:i:s")
        ]);

        $this->info("successful!");
    }
}
