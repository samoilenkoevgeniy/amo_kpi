<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        "key", "value"
    ];

    public $timestamps = false;

    /**
     * @param $key
     * @param $value
     */
    public static function set($key, $value)
    {
        $setting = Setting::whereKey($key)->first();
        if($setting) {
            $setting->value = $value;
            $setting->save();
        } else {
            Setting::create([
                "key" => $key,
                "value" => $value
            ]);
        }
    }

    /**
     * @param $key
     * @return null
     */
    public static function get($key)
    {
        $setting = Setting::whereKey($key)->first();
        if($setting) {
            return $setting->value;
        } else {
            return null;
        }
    }
}
