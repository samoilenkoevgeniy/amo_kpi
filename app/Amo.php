<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/*
 * @todo refactoring
 * */
class Amo extends Model
{
    public static function auth()
    {
        $user = [
            'USER_LOGIN' => ENV("AMO_USER_LOGIN"),
            'USER_HASH' => ENV("AMO_USER_HASH")
        ];

        $link='https://'.ENV("AMO_SUBDOMAIN").'.amocrm.ru/private/api/auth.php?type=json';
        $curl=curl_init();

        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($curl,CURLOPT_URL,$link);
        curl_setopt($curl,CURLOPT_POST,true);
        curl_setopt($curl,CURLOPT_POSTFIELDS, http_build_query($user));
        curl_setopt($curl,CURLOPT_HEADER,false);
        curl_setopt($curl,CURLOPT_COOKIEFILE, storage_path('app/public/cookie.txt'));
        curl_setopt($curl,CURLOPT_COOKIEJAR, storage_path('app/public/cookie.txt'));
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);

        $out=curl_exec($curl);
        $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
        curl_close($curl);

        $Response=json_decode($out,true);
        $Response=$Response['response'];
        if (!isset($Response['auth'])) {
            \Log::error("AmoCRM Auth falied. Cpde:".$code);
        }
    }

    /**
     * @deprecated
     * @return string
     */
    public static function cookie_root_path()
    {
        return explode("/public",$_SERVER['DOCUMENT_ROOT'])[0].'/storage/app/public/cookie.txt';
    }

    /**
     * @return mixed
     */
    public static function account_current()
    {
        $link='https://'.ENV("AMO_SUBDOMAIN").'.amocrm.ru/private/api/v2/json/accounts/current';
        $curl=curl_init();
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($curl,CURLOPT_URL,$link);
        curl_setopt($curl,CURLOPT_HEADER,false);
        curl_setopt($curl,CURLOPT_COOKIEFILE, storage_path('app/public/cookie.txt'));
        curl_setopt($curl,CURLOPT_COOKIEJAR, storage_path('app/public/cookie.txt'));
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);

        $out=curl_exec($curl);
        curl_close($curl);

        $Response=json_decode($out,true);
        $account = $Response['response']['account']['users'];

        return $account;
    }

    /**
     * @param string $date
     * @return array|mixed
     */
    public static function get_events($date = "")
    {

//        $link = 'https://' . ENV("AMO_SUBDOMAIN") . '.amocrm.ru/private/api/v2/json/notes/list?note_type=11&type=contact&limit_rows=500';
//        $link2 = 'https://' . ENV("AMO_SUBDOMAIN") . '.amocrm.ru/private/api/v2/json/notes/list?note_type=10&type=contact&limit_rows=500';
//
//        $curl = curl_init();
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
//        if($date) {
//            curl_setopt($curl,CURLOPT_HTTPHEADER, ['IF-MODIFIED-SINCE: '.$date]);
//        }
//        curl_setopt($curl, CURLOPT_URL, $link);
//        curl_setopt($curl, CURLOPT_HEADER, false);
//        curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('app/public/cookie.txt'));
//        curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('app/public/cookie.txt'));
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
//        $out = curl_exec($curl);
//
//        $curl = curl_init();
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
//        curl_setopt($curl,CURLOPT_HTTPHEADER, ['IF-MODIFIED-SINCE: '.$date]);
//        curl_setopt($curl, CURLOPT_URL, $link2);
//        curl_setopt($curl, CURLOPT_HEADER, false);
//        curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('app/public/cookie.txt'));
//        curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('app/public/cookie.txt'));
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
//        $out2 = curl_exec($curl);
//
//        $Response=json_decode($out,true);
//        $Response2=json_decode($out2,true);
//
//        $out=$Response['response']['notes'];
//        $out2=$Response2['response']['notes'];

        $result = [];
        $offset = 0;
        $key_end = 0;
        $counter = 0;
        while($key_end != 1) {
            $link = 'https://' . ENV("AMO_SUBDOMAIN") . '.amocrm.ru/private/api/v2/json/notes/list?note_type=11&type=contact&limit_rows=500&limit_offset='.$offset;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['IF-MODIFIED-SINCE: '.$date]);
            curl_setopt($curl, CURLOPT_URL, $link);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('app/public/cookie.txt'));
            curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('app/public/cookie.txt'));
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            $out = curl_exec($curl);
            $out=json_decode($out,true);
            if(is_array($out)) {
                $result = array_merge($result, $out['response']['notes']);
            }
            $offset += 500;
            if(!is_array($out)) {
                $key_end = 1;
            }
            $counter++;
            usleep(50000);
        }

        $offset = 0;
        $key_end = 0;
        while($key_end != 1) {
            $link = 'https://' . ENV("AMO_SUBDOMAIN") . '.amocrm.ru/private/api/v2/json/notes/list?note_type=10&type=contact&limit_rows=500&limit_offset='.$offset;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['IF-MODIFIED-SINCE: '.$date]);
            curl_setopt($curl, CURLOPT_URL, $link);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('app/public/cookie.txt'));
            curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('app/public/cookie.txt'));
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            $out = curl_exec($curl);
            $out=json_decode($out,true);
            if(is_array($out)) {
                $result = array_merge($result, $out['response']['notes']);
            }
            $offset += 500;
            if(!is_array($out)) {
                $key_end = 1;
            }
            $counter++;
            usleep(50000);
        }

        /*
        $sync->count_incoming = count($out);
        $sync->count_outgoing = count($out);
        $sync->save();*/

        return $result ? $result : [];
    }

    /**
     * @return array
     */
    public static function get_events_all()
    {
        $result = [];
        $offset = 0;
        $key_end = 0;
        $counter = 0;
        while($key_end != 1) {
            $link = 'https://' . ENV("AMO_SUBDOMAIN") . '.amocrm.ru/private/api/v2/json/notes/list?note_type=11&type=contact&limit_rows=500&limit_offset='.$offset;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
            curl_setopt($curl, CURLOPT_URL, $link);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('app/public/cookie.txt'));
            curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('app/public/cookie.txt'));
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            $out = curl_exec($curl);
            $out=json_decode($out,true);
            if(is_array($out)) {
                $result = array_merge($result, $out['response']['notes']);
            }
            $offset += 500;
            if(!is_array($out)) {
                $key_end = 1;
            }
            $counter++;
            usleep(50000);
        }

        $offset = 0;
        $key_end = 0;
        while($key_end != 1) {
            $link = 'https://' . ENV("AMO_SUBDOMAIN") . '.amocrm.ru/private/api/v2/json/notes/list?note_type=10&type=contact&limit_rows=500&limit_offset='.$offset;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
            curl_setopt($curl, CURLOPT_URL, $link);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('app/public/cookie.txt'));
            curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('app/public/cookie.txt'));
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            $out = curl_exec($curl);
            $out=json_decode($out,true);
            if(is_array($out)) {
                $result = array_merge($result, $out['response']['notes']);
            }
            $offset += 500;
            if(!is_array($out)) {
                $key_end = 1;
            }
            $counter++;
            usleep(50000);
        }

        $settings = [
            "success_criteria" => \Redis::get("system.settings.success_criteria")
        ];


        foreach($result as $item) {

            $amo_id = $item['created_user_id'];
            $created_at = Carbon::createFromTimestampUTC($item['date_create'])->format('Y-m-d H:i:s');
            $text = json_decode($item['text'],1);
            $duration = $text['DURATION'];
            $external_id = $text['UNIQ'];
            $link = array_key_exists("LINK", $text) ? $text['LINK'] : "";
            $phone = array_key_exists("PHONE", $text) ? $text['PHONE'] : "";
            $call_status = array_key_exists("call_status", $text) ? $text['call_status'] : "";
            $user_id = \Redis::get("user.amo_id.".$amo_id);

            if(!$user_id) {
                $user = User::whereAmoId($amo_id)->first();
                if($user) {
                    \Redis::set("user.amo.id.".$user->amo_id, $user->id);
                    $user_id = $user->id;
                }
            }

            $dialing = ($call_status === 4) || $duration > (int)\Redis::get("system.settings.dialing_success_criteria") ? 1 : 0;
            //$dialing = ( ? 1 : 0;

            UserCall::create([
                "user_id" => $user_id,
                "date" => $created_at,
                "duration" => $duration,
                "link" => $link,
                "external_id" => $external_id,
                "incoming" => ($item['note_type'] == 10) ? 1 : 0,
                "amo_id" => $amo_id,
                "phone" => $phone,
                "success" => ((int)$duration > $settings['success_criteria']) ? 1 : 0,
                "dialing" => $dialing
            ]);

        }

        return $result;
    }

    /**
     *
     */
    public static function getUsers()
    {
        Amo::auth();
        $users = Amo::account_current();
        foreach($users as $user) {
            $data = [
                "name" => $user['name'],
                "email" => $user['login'],
                "amo_id" => $user['id'],
                "role_id" => 3
            ];
            if($user['is_admin']) {
                $data['role_id'] = 1;
            }
            User::create($data);
        }
    }

    /**
     * @param $date
     */
    public static function getCalls($date = "")
    {
        Amo::auth();

        $items = Amo::get_events_all();

        $settings = [
            "success_criteria" => \Redis::get("system.settings.success_criteria")
        ];

        foreach($items as $item) {

            $amo_id = $item['created_user_id'];
            $created_at = Carbon::createFromTimestampUTC($item['date_create'])->format('Y-m-d H:i:s');
            $text = json_decode($item['text'],1);
            $duration = $text['DURATION'];
            $external_id = $text['UNIQ'];
            $link = array_key_exists("LINK", $text) ? $text['LINK'] : "";
            $phone = array_key_exists("PHONE", $text) ? $text['PHONE'] : "";
            $call_status = array_key_exists("call_status", $text) ? $text['call_status'] : "";
            $user_id = \Redis::get("user.amo_id.".$amo_id);

            if(!$user_id) {
                $user = User::whereAmoId($amo_id)->first();
                if($user) {
                    \Redis::set("user.amo.id.".$user->amo_id, $user->id);
                    $user_id = $user->id;
                }
            }

            UserCall::create([
                "user_id" => $user_id,
                "date" => $created_at,
                "duration" => $duration,
                "link" => $link,
                "external_id" => $external_id,
                "incoming" => ($item['note_type'] == 10) ? 1 : 0,
                "amo_id" => $amo_id,
                "phone" => $phone,
                "success" => ((int)$duration > $settings['success_criteria']) ? 1 : 0,
                "dialing" => ((int)$call_status === 4) ? 1 : 0
            ]);

        }
    }

    public static function getCallsConsole($date = "")
    {
        Amo::auth();

        $items = Amo::get_events($date);

        $settings = [
            "success_criteria" => \Redis::get("system.settings.success_criteria")
        ];

        foreach($items as $item) {

            $amo_id = $item['created_user_id'];
            $created_at = Carbon::createFromTimestampUTC($item['date_create'])->format('Y-m-d H:i:s');
            $text = json_decode($item['text'],1);
            $duration = array_key_exists("DURATION", $text) ? $text['DURATION'] : 0;
            $external_id = array_key_exists("UNIQ", $text) ? $text['UNIQ'] : "";
            $link = array_key_exists("LINK", $text) ? $text['LINK'] : "";
            $phone = array_key_exists("PHONE", $text) ? $text['PHONE'] : "";
            $call_status = array_key_exists("call_status", $text) ? $text['call_status'] : "";
            $user_id = \Redis::get("user.amo_id.".$amo_id);

            if(!$user_id) {
                $user = User::whereAmoId($amo_id)->first();
                if($user) {
                    \Redis::set("user.amo.id.".$user->amo_id, $user->id);
                    $user_id = $user->id;
                }
            }

            UserCall::create([
                "user_id" => $user_id,
                "date" => $created_at,
                "duration" => $duration,
                "link" => $link,
                "external_id" => $external_id,
                "incoming" => ($item['note_type'] == 10) ? 1 : 0,
                "amo_id" => $amo_id,
                "phone" => $phone,
                "success" => ((int)$duration > $settings['success_criteria']) ? 1 : 0,
                "dialing" => ((int)$call_status === 4) ? 1 : 0
            ]);

        }
    }

    public static function getDealsAndTasks($date)
    {
        Amo::auth();

        $result = [];
        $offset = 0;
        $key_end = 0;
        $counter = 0;
        while($key_end != 1) {
            $link = 'https://' . ENV("AMO_SUBDOMAIN") . '.amocrm.ru/private/api/v2/json/leads/list?limit_rows=500&limit_offset='.$offset;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['IF-MODIFIED-SINCE: '.$date]);
            curl_setopt($curl, CURLOPT_URL, $link);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('app/public/cookie.txt'));
            curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('app/public/cookie.txt'));
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            $out = curl_exec($curl);
            $out = json_decode($out, 1);
            if(is_array($out)) {
                $result = array_merge($result, $out['response']['leads']);
            }
            $offset += 500;
            if(!is_array($out)) {
                $key_end = 1;
            }
            $counter++;
            usleep(50000);
        }

//        $link = 'https://' . ENV("AMO_SUBDOMAIN") . '.amocrm.ru/private/api/v2/json/leads/list?limit_rows=500';
//        $curl = curl_init();
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
//        curl_setopt($curl,CURLOPT_HTTPHEADER, ['IF-MODIFIED-SINCE: '.$date]);
//        curl_setopt($curl, CURLOPT_URL, $link);
//        curl_setopt($curl, CURLOPT_HEADER, false);
//        curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('app/public/cookie.txt'));
//        curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('app/public/cookie.txt'));
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
//        $out = curl_exec($curl);
//        $items = json_decode($out, 1);
//        $items_deals = $items ? $items : [];

        foreach($result as $item) {
            $close = 0;
            $closed_at = NULL;
            $amo_id = array_key_exists("responsible_user_id", $item) ? $item['responsible_user_id'] : NULL;
            $user_id = User::getUserByAmoId($amo_id);
            if(array_key_exists("date_create", $item)) {
                $created_at = Carbon::createFromTimestampUTC($item['date_create'])->format('Y-m-d H:i:s');
            }
            if(array_key_exists("date_close", $item) && $item['date_close']) {
                $closed_at = Carbon::createFromTimestampUTC($item['date_close'])->format('Y-m-d H:i:s');
                $close = 1;
            }

            Deal::create([
                "user_id" => $user_id,
                "name" => $item['name'],
                "status_id" => $item['status_id'],
                "closed" => $close,
                "date_close" => $closed_at,
                "date_create" => $created_at,
                "external_id" => $item['id']
            ]);
        }

//        $link = 'https://' . ENV("AMO_SUBDOMAIN") . '.amocrm.ru/private/api/v2/json/tasks/list?type=lead&limit_rows=500';
//        $curl = curl_init();
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
//        curl_setopt($curl,CURLOPT_HTTPHEADER, ['IF-MODIFIED-SINCE: '.$date]);
//        curl_setopt($curl, CURLOPT_URL, $link);
//        curl_setopt($curl, CURLOPT_HEADER, false);
//        curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('app/public/cookie.txt'));
//        curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('app/public/cookie.txt'));
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
//        $out = curl_exec($curl);
//        $items = json_decode($out, 1);
//        $items_tasks = $items ? $items : [];

        $result = [];
        $offset = 0;
        $key_end = 0;
        $counter = 0;
        while($key_end != 1) {
            $link = 'https://' . ENV("AMO_SUBDOMAIN") . '.amocrm.ru/private/api/v2/json/tasks/list?limit_rows=500&limit_offset='.$offset;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['IF-MODIFIED-SINCE: '.$date]);
            curl_setopt($curl, CURLOPT_URL, $link);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('app/public/cookie.txt'));
            curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('app/public/cookie.txt'));
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            $out = curl_exec($curl);
            $out = json_decode($out, 1);
            if(is_array($out)) {
                $result = array_merge($result, $out['response']['tasks']);
            }
            $offset += 500;
            if(!is_array($out)) {
                $key_end = 1;
            }
            $counter++;
            usleep(50000);
        }

        foreach($result as $item) {
            $close = 0;
            $closed_at = NULL;
            $amo_id = $item['responsible_user_id'];
            $user_id = User::getUserByAmoId($amo_id);
            $closed_at = Carbon::createFromTimestampUTC($item['last_modified'])->format('Y-m-d H:i:s');
            $created_at = Carbon::createFromTimestampUTC($item['date_create'])->format('Y-m-d H:i:s');
            $date_due = Carbon::createFromTimestampUTC($item['complete_till'])->format('Y-m-d H:i:s');
            if($item['status']) {
                $close = 1;
            }
            $deal = Deal::whereExternalId($item['element_id'])->first();
            if($deal) {
                Task::create([
                    "user_id" => $user_id,
                    "text" => $item['text'],
                    "closed" => $close,
                    "date_last_modify" => $closed_at,
                    "date_create" => $created_at,
                    "date_due" => $date_due,
                    "task_type" => $item['task_type'],
                    "deal_id" => $deal->id
                ]);
            }
        }
    }

    /**
     * @todo make getting all events with offsets
     * for initialization
     */
    public static function getAllCalls()
    {
        Amo::auth();

        $items = Amo::get_events();

        $settings = [
            "success_criteria" => \Redis::get("system.settings.success_criteria")
        ];

        foreach($items as $item) {

            $amo_id = $item['created_user_id'];
            $created_at = Carbon::createFromTimestampUTC($item['date_create'])->format('Y-m-d H:i:s');
            $text = json_decode($item['text'],1);
            $duration = $text['DURATION'];
            $external_id = $text['UNIQ'];
            $link = array_key_exists("LINK", $text) ? $text['LINK'] : "";
            $phone = array_key_exists("PHONE", $text) ? $text['PHONE'] : "";
            $call_status = array_key_exists("call_status", $text) ? $text['call_status'] : "";
            $user_id = User::getUserByAmoId($amo_id);
            UserCall::create([
                "user_id" => $user_id,
                "date" => $created_at,
                "duration" => $duration,
                "link" => $link,
                "external_id" => $external_id,
                "incoming" => ($item['note_type'] == 10) ? 1 : 0,
                "amo_id" => $amo_id,
                "phone" => $phone,
                "success" => ((int)$duration > $settings['success_criteria']) ? 1 : 0,
                "dialing" => ((int)$call_status === 4) ? 1 : 0
            ]);

        }
    }

    /**
     *
     */
    public static function getAllDeals()
    {
        Amo::auth();
        $result = [];
        $offset = 0;
        $key_end = 0;
        $counter = 0;
        while($key_end != 1) {
            $link = 'https://' . ENV("AMO_SUBDOMAIN") . '.amocrm.ru/private/api/v2/json/leads/list?limit_rows=500&limit_offset='.$offset;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
            curl_setopt($curl, CURLOPT_URL, $link);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('app/public/cookie.txt'));
            curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('app/public/cookie.txt'));
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            $out = curl_exec($curl);
            $out = json_decode($out, 1);
            if(is_array($out)) {
                $result = array_merge($result, $out['response']['leads']);
            }
            $offset += 500;
            if(!is_array($out)) {
                $key_end = 1;
            }
            $counter++;
            usleep(50000);
        }

        foreach($result as $item) {
            $close = 0;
            $closed_at = NULL;
            $amo_id = $item['responsible_user_id'];
            $user_id = User::getUserByAmoId($amo_id);
            $created_at = Carbon::createFromTimestampUTC($item['date_create'])->format('Y-m-d H:i:s');
            if($item['date_close']) {
                $closed_at = Carbon::createFromTimestampUTC($item['date_close'])->format('Y-m-d H:i:s');
                $close = 1;
            }

            Deal::create([
                "user_id" => $user_id,
                "name" => $item['name'],
                "status_id" => $item['status_id'],
                "closed" => $close,
                "date_close" => $closed_at,
                "date_create" => $created_at,
                "external_id" => $item['id']
            ]);
        }
    }

    /**
     *
     */
    public static function getAllTasks()
    {
        Amo::auth();
        $result = [];
        $offset = 0;
        $key_end = 0;
        $counter = 0;
        while($key_end != 1) {
            $link = 'https://' . ENV("AMO_SUBDOMAIN") . '.amocrm.ru/private/api/v2/json/tasks/list?type=lead&limit_rows=500&limit_offset='.$offset;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
            curl_setopt($curl, CURLOPT_URL, $link);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('app/public/cookie.txt'));
            curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('app/public/cookie.txt'));
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            $out = curl_exec($curl);
            $out = json_decode($out, 1);

            if(is_array($out)) {
                $result = array_merge($result, $out['response']['tasks']);
            }
            $offset += 500;
            if(!is_array($out)) {
                $key_end = 1;
            }
            $counter++;
            usleep(50000);
        }

        foreach($result as $item) {
            $close = 0;
            $closed_at = NULL;
            $amo_id = $item['responsible_user_id'];
            $user_id = User::getUserByAmoId($amo_id);
            $closed_at = Carbon::createFromTimestampUTC($item['last_modified'])->format('Y-m-d H:i:s');
            $created_at = Carbon::createFromTimestampUTC($item['date_create'])->format('Y-m-d H:i:s');
            $date_due = Carbon::createFromTimestampUTC($item['complete_till'])->format('Y-m-d H:i:s');
            if($item['status']) {
                $close = 1;
            }

            $deal = Deal::whereExternalId($item['element_id'])->first();

            if($deal) {
                Task::create([
                    "user_id" => $user_id,
                    "text" => $item['text'],
                    "closed" => $close,
                    "date_last_modify" => $closed_at,
                    "date_create" => $created_at,
                    "date_due" => $date_due,
                    "task_type" => $item['task_type'],
                    "deal_id" => $deal->id
                ]);
            }
        }
    }

    /**
     *
     */
    public static function getOldDailyStatistics()
    {
        $deals = Deal::whereNotNull("user_id")->get();
        $result = [];
        foreach($deals as $deal) {
            $key = explode(" ", $deal->date_create)[0];
            $result[$key]["deals"][$deal->user_id][] = $deal;
        }
        $tasks = Task::whereNotNull("user_id")->get();
        foreach($tasks as $task) {
            $key = explode(" ", $task->date_create)[0];
            $result[$key]["tasks"][$task->user_id][] = $task;
        }
        $calls = UserCall::whereNotNull("user_id")->get();
        foreach($calls as $call) {
            $key = explode(" ", $call->date)[0];
            $result[$key]["calls"][$call->user_id][] = $call->toArray();
        }

        $users = User::all();
        foreach($result as $key=>$value) {
            $key_date = Carbon::createFromFormat("Y-m-d", $key);
            $key_date->hour = 0;
            $key_date->minute = 0;
            $key_date->second = 0;
            $key_date_start = $key_date->format("Y-m-d H:i:s");
            $key_date_end = $key_date->addDay()->format("Y-m-d H:i:s");

            $max_average_diff = (int)\Redis::get("system.settings.max_average_diff");
            $date_start = $key." ".\Redis::get("system.settings.time_work_day_start").":00";
            $date_end = $key." ".\Redis::get("system.settings.time_work_day_end").":00";

            //dd($date_start. " " .$date_end);

            foreach($users as $user) {

                $total_penalty = 0;
                $diff = 0;
                $average_diff = 0;
                $first_call = NULL;
                $last_call = NULL;
                $total_salary = 0;

                if(array_key_exists("calls", $value)) {
                    $calls = $value['calls'];

                    if(array_key_exists($user->id, $calls)) {

                        $count_calls = count($calls[$user->id]);
                        if($count_calls > 1) {
                            for($i = 0; $i != $count_calls-1; $i++) {
                                $dt = Carbon::createFromFormat("Y-m-d H:i:s", $calls[$user->id][$i]['date']);
                                $dt_next = Carbon::createFromFormat("Y-m-d H:i:s", $calls[$user->id][$i+1]['date']);
                                $diff += $dt->diffInMinutes($dt_next);
                            }
                        }
                        $average_diff = $count_calls ? $diff/$count_calls : 0;

                        if($count_calls > 0) {
                            usort($calls[$user->id], function($a, $b){
                                return ($a['date'] > $b['date']);
                            });
                            $calls_tmp = $calls[$user->id];
                            $calls_tmp_2 = $calls[$user->id];
                            $first_call = explode(" ", array_shift($calls_tmp)['date'])[1];
                            $last_call = explode(" ", array_pop($calls_tmp_2)['date'])[1];


                            $dt_first_call = Carbon::createFromFormat("Y-m-d H:i:s", $key." ".$first_call);
                            $dt_last_call = Carbon::createFromFormat("Y-m-d H:i:s", $key." ".$last_call);

                            $dt_day_start = Carbon::createFromFormat("Y-m-d H:i:s", $date_start);
                            $dt_day_end = Carbon::createFromFormat("Y-m-d H:i:s", $date_end);

                            $diff_first_call = $dt_first_call->gt($dt_day_start) ? $dt_day_start->diffInMinutes($dt_first_call) : 0;
                            $diff_last_call =  $dt_last_call->gt($dt_day_end) ? 0 : $dt_day_end->diffInMinutes($dt_last_call);

                            $total_penalty += $diff_first_call*\Redis::get("system.settings.penalty_first_call");
                            $total_penalty += $diff_last_call*\Redis::get("system.settings.penalty_last_call");
                        }

                        if($average_diff > $max_average_diff) {
                            $penalty_time = abs($max_average_diff-$average_diff);
                            $total_penalty += $penalty_time*(int)\Redis::get("system.settings.penalty_average_diff");
                        }

                        $dialing_calls_all = UserCall::whereSuccess(1)->whereUserId($user->id)->where(function($query) use ($key_date_start, $key_date_end){
                            $query->where("date", ">", $key_date_start);
                            $query->where("date", "<", $key_date_end);
                        })->get()->count();

                        $success_calls_total = UserCall::whereSuccess(1)->whereUserId($user->id)->where(function($query) use ($key_date_start, $key_date_end){
                            $query->where("date", ">", $key_date_start);
                            $query->where("date", "<", $key_date_end);
                        })->get()->count();

                        $success_calls_total_duration = UserCall::whereSuccess(1)->whereUserId($user->id)->where(function($query) use ($key_date_start, $key_date_end){
                            $query->where("date", ">", $key_date_start);
                            $query->where("date", "<", $key_date_end);
                        })->sum("duration");

                        $total_salary = round(($dialing_calls_all*(int)\Redis::get("system.settings.pay_dialing")) +
                            ($success_calls_total*(int)\Redis::get("system.settings.pay_dialing_success")) +
                            (($success_calls_total_duration/60)*(int)\Redis::get("system.settings.pay_successful_conversation"))
                            , 2);
                    }
                }

                $count_errors = 0;
                if(array_key_exists("deals", $value)) {
                    $deals = $value['deals'];
                    if(array_key_exists($user->id, $deals)) {
                        foreach($deals[$user->id] as $deal) {
                            //@todo penalty
                        }
                    }
                }

                $overdue_tasks = 0;
                if(array_key_exists("tasks", $value)) {
                    $tasks = $value['tasks'];
                    if(array_key_exists($user->id, $tasks)) {
                        foreach($tasks[$user->id] as $task) {
                            if($task->date_last_modify > $task->date_due) {
                                $overdue_tasks++;
                            }
                        }
                        $total_penalty+= $overdue_tasks*\Redis::get("system.settings.penalty_overdue_task");
                    }
                }


                DailyStatistic::create([
                    "date" => $key,
                    "average_diff" => $average_diff,
                    "first_call" => $first_call,
                    "last_call" => $last_call,
                    "user_id" => $user->id,
                    "open_deals_with_out_tasks" => $count_errors,
                    "overdue_tasks" => $overdue_tasks,
                    "total_penalty" => $total_penalty,
                    "total_salary" => $total_salary
                ]);
            }
        }

        return "successful!";
    }
}
