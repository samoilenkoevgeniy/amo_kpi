<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_calls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id", false, true)->nullable();
            $table->integer("amo_id", false, true)->nullable();
            $table->timestamp("date");
            $table->integer("duration");
            $table->text("external_id");
            $table->text("link");
            $table->text("phone");
            $table->boolean("incoming");
            $table->boolean("success");
            $table->boolean("dialing");
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_calls');
    }
}
