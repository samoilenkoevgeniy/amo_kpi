<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->increments('id');
            $table->text("name");
            $table->integer("external_id", false, true);
            $table->integer("user_id", false, true)->nullable();
            $table->integer("status_id", false, true);
            $table->boolean("closed");
            $table->timestamp("date_close")->nullable();
            $table->timestamp("date_create");
            $table->timestamps();
            $table->foreign("user_id")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deals');
    }
}
