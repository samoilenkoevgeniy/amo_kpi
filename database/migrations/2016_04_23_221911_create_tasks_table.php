<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->text("task_type");
            $table->text("text");
            $table->integer("user_id", false, true);
            $table->boolean("closed");
            $table->date("date_create");
            $table->date("date_last_modify");
            $table->date("date_due");
            $table->integer("deal_id", false, true);
            $table->foreign("user_id")->references("id")->on("users");
            $table->foreign("deal_id")->references("id")->on("deals")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasks');
    }
}
