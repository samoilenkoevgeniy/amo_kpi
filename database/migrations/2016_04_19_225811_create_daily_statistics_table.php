<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->date("date");
            $table->float("average_diff");
            $table->time("first_call")->nullable();
            $table->time("last_call")->nullable();
            $table->integer("open_deals_with_out_tasks");
            $table->integer("overdue_tasks");
            $table->integer("total_penalty", false, true);
            $table->integer("user_id", false, true);
            $table->foreign("user_id")->references("id")->on("users");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('daily_statistics');
    }
}
