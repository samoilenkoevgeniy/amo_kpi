<?php

use Illuminate\Database\Seeder;

class Roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Role::create([
            "name" => "Администратор",
            "slug" => "administrator"
        ]);

        \App\Role::create([
            "name" => "Руководитель отдела",
            "slug" => "head_manager"
        ]);

        \App\Role::create([
            "name" => "Менеджер",
            "slug" => "manager"
        ]);
    }
}
