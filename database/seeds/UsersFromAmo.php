<?php

use Illuminate\Database\Seeder;

class UsersFromAmo extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Amo::auth();
        $users = \App\Amo::account_current();
        foreach($users as $user) {
            $data = [
                "name" => $user['name'],
                "email" => $user['login'],
                "amo_id" => $user['id'],
                "role_id" => 3
            ];
            if($user['is_admin']) {
                $data['role_id'] = 1;
            }
            App\User::create($data);
        }
    }
}
