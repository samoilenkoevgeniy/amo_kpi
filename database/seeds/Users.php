<?php

use Illuminate\Database\Seeder;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            "name" => "Администратор",
            "email" => "admin@system.ru",
            "password" => Hash::make("123123"),
            "role_id" => 1
        ]);


        \App\Amo::getUsers();
    }
}
