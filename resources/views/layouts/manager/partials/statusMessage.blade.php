<div class="col-lg-12 col-md-12">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
            {{ \Session::forget("status") }}
        </div>
    @endif
</div>