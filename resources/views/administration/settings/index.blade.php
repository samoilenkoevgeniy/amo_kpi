@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                @include("layouts.administration.partials.statusMessage")
            </div>
        </div>
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">Настройки</div>
                <div class="panel-body">
                    <form action="{{ url("/administration/settings/update") }}" method="post">
                        {!! csrf_field() !!}
                        @foreach($fields as $key => $field)
                            <div class="col-md-4 col-lg-4">
                                <div class="panel panel-default">
                                    <div class="panel-heading">{{ $field['rus_name'] }}</div>
                                    <div class="panel-body">
                                        <input class="form-control" type="text" name="{{ $key }}" value="{{ \Redis::get("system.settings.".$key) }}" />
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="col-md-12 col-lg-12">
                            <div class="form-group">
                                <input class="btn btn-primary" type="submit" value="Сохранить значения" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection