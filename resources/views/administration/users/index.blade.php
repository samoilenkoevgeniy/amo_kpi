@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            @include("layouts.administration.partials.statusMessage")
            <div class="panel panel-default">
                <div class="panel-heading">Список пользователей</div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Имя пользователя</th>
                        <th>Email</th>
                        <th>Роль</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->role->name }}</td>
                            <td><a href="{{ url("/administration/users/dropPassword/".$user->id) }}">Сбросить пароль</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection