@extends("layouts.app")

@section("content")
<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="pull-left">
                        Статистика
                    </div>
                    <div class="pull-left">
                        &nbsp; <a href="{{ url("/manager/statistics") }}" class="btn btn-primary btn-xs @if($today != 1 && $timestamp == "") active @endif">За все время</a>
                        &nbsp; <a href="{{ url("/manager/statistics/?today=1") }}" class="btn btn-primary btn-xs @if(($today == 1)) active @endif">За сегодня</a>
                        &nbsp; <button id="datepicker" data-url-fallback="/manager/statistics" href="#" class="btn btn-primary btn-xs @if($timestamp!="") active @endif">За дату</button>
                        &nbsp;<strong>{{ $date_for_view }}</strong>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">Общая статистика по звонкам</div>

                                <div class="panel-body">
                                    <canvas id="dialingAndNoCalls" style="width: 100%; height: 320px;"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">Всего минут к успешным минутам</div>

                                <div class="panel-body">
                                    <canvas id="allCallMinutes" style="width: 100%; height: 320px;"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            @if(($timestamp != "") && $dailyStatistic)
                                <div class="well">
                                    <h4>Время первого звонка: {{ $dailyStatistic->first_call }}</h4>
                                </div>

                                <div class="well">
                                    <h4>Время последнего звонка: {{ $dailyStatistic->last_call }}</h4>
                                </div>

                                <div class="well">
                                    <h4>Среднее время между звонками: <strong>{{ $average_diff }}</strong></h4>
                                </div>
                            @elseif($today && (date("H:i") < \Redis::get("system.settings.time_work_day_end")))
                                <h3>Статистика будет сформирована в конце рабочего дня</h3>
                            @endif
                            <table class="table table-hover">
                                <tr class="success">
                                    <td>Вы заработали</td>
                                    <td>{{ $total_salary }} рублей</td>
                                </tr>
                                <tr class="danger">
                                    <td>Штрафы</td>
                                    <td>{{ $total_penalty }} рублей</td>
                                </tr>
                                <tr>
                                    <td><strong>Итого</strong></td>
                                    <td>{{ $total_salary_diff }} рублей</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Детализация по звонкам</div>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Дата</th>
                                <th>Результат</th>
                                <th>Продолжительность(мин.)</th>
                                <th>Тип звонка</th>
                                <th>Оплата за звонок</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($calls as $call)
                                <?php $cost = $call->getCostCall(); ?>
                                <tr {{ ($cost > 0) ? "class=success" : ""}}>
                                    <td>{{ $call->date }}</td>
                                    <td>
                                        @if($call->success)
                                            Успешно
                                        @else
                                            Не успешно ({{ $call->dialing ? "Дозвонился" : "Не дозвонился" }})
                                        @endif
                                    </td>
                                    <td>{{ round($call->duration/60, 2) }}</td>
                                    <td>{{ $call->incoming ? "Входящий" : "Исходящий" }}</td>
                                    <td>
                                        {{ $call->getCostCall() }} руб.
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if($today == 0 && $timestamp == "")
                            <div class="panel-body">
                                {!! $calls->render() !!}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("scripts")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
    <script>

        var ctx2 = document.getElementById("dialingAndNoCalls").getContext("2d");
        var myLineChart2 = new Chart(ctx2).Pie({!! $data_pie !!}, []);

        var ctx3 = document.getElementById("allCallMinutes").getContext("2d");
        var myLineChart3 = new Chart(ctx3).Pie({!! $data_pie_2 !!}, []);
    </script>
@endsection