@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="pull-left">
                        Статистика
                    </div>
                    <div class="pull-left">
                        &nbsp; <a href="/" class="btn btn-primary btn-xs @if($today != 1 && $date_for_view == "") active @endif">За все время</a>
                        &nbsp; <a href="/?today=1" class="btn btn-primary btn-xs @if(($today == 1)) active @endif">За сегодня</a>
                        &nbsp; <button id="datepicker" data-url-fallback="/" href="#" class="btn btn-primary btn-xs @if($date_for_view!="") active @endif">За дату</button>
                        &nbsp;<strong>{{ $date_for_view }}</strong>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body" style="overflow-x: scroll;">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Пользователь</th>
                            <th>Всего звонков</th>
                            <th>Дозвонов</th>
                            <th>Недозвонов</th>
                            <th>Успешных звонков</th>
                            <th>Качество базы</th>
                            <th>Продолжительность успешных звонков (мин.)</th>
                            <th>Средняя длина успешных разговоров (мин.)</th>
                            @if($today != 0 || $timestamp != "")
                                <th>Среднее время между разговорами (мин.)</th>
                                <th>Первый звонок</th>
                                <th>Последний звонок</th>
                            @endif
                            <th>Итого штраф (руб.)</th>
                            <th>Итого зарплата (руб.)</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $item)
                            <tr>
                                <td>{{ $item['user_name'] }}</td>
                                <td>{{ $item['count_calls'] }}</td>
                                <td>{{ $item['dialing'] }}</td>
                                <td>{{ $item['not_dialing'] }}</td>
                                <td>{{ $item['count_calls_success'] }}</td>
                                <td>{{ $item['quality_base'] }}%</td>
                                <td>{{ $item['duration_success_calls'] }}</td>
                                <td>{{ $item['average_length_success_calls'] }}</td>
                                @if($today != 0 || $timestamp != "")
                                    <td>{{ $item['average_diff'] }}</td>
                                    <td>{{ $item['first_call'] }}</td>
                                    <td>{{ $item['last_call'] }}</td>
                                @endif
                                <td><span class="money">{{ $item['total_penalty'] }}</span></td>
                                <td><span class="money">{{ $item['total_salary'] }}</span></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Количество звонков к успешным звонкам по менеджерам</div>

                <div class="panel-body">
                    <canvas id="countCalls" style="width: 100%; height: 238px;"></canvas>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Отношение звонков к дозвонам и успешным звонкам</div>

                <div class="panel-body">
                    <h3>Общее качество базы: {{ $total_quality_base }}%</h3>
                    <canvas id="dialingAndNoCalls" style="width: 100%; height: 200px;"></canvas>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Всего минут к успешным минутам</div>

                <div class="panel-body">
                    <canvas id="allCallMinutes" style="width: 100%; height: 255px;"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("scripts")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
    <script>
        /*var data = [
            labels : {!! $labels_bar_chart !!}},
            datasets :
        ];*/
        var data = {
            labels: [{!! $labels_bar_chart !!}],
            datasets: [
                {
                    label: "My First dataset",
                    fillColor: "rgba(220,220,220,0.5)",
                    strokeColor: "rgba(220,220,220,0.8)",
                    highlightFill: "rgba(220,220,220,0.75)",
                    highlightStroke: "rgba(220,220,220,1)",
                    data: {!! $bar_chart_call_all !!}
                },
                {
                    label: "My Second dataset",
                    fillColor: "rgba(151,187,205,0.5)",
                    strokeColor: "rgba(151,187,205,0.8)",
                    highlightFill: "rgba(151,187,205,0.75)",
                    highlightStroke: "rgba(151,187,205,1)",
                    data: {!! $bar_chart_call_success !!}
                }
            ]
        };

        var ctx = document.getElementById("countCalls").getContext("2d");
        var myLineChart = new Chart(ctx).Bar(data, []);

        var ctx2 = document.getElementById("dialingAndNoCalls").getContext("2d");
        var myLineChart2 = new Chart(ctx2).Pie({!! $data_pie !!}, []);

        var ctx3 = document.getElementById("allCallMinutes").getContext("2d");
        var myLineChart3 = new Chart(ctx3).Pie({!! $data_pie_2 !!}, []);

    </script>
@endsection